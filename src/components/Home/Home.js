import React, {Component} from 'react';
import Header from './../Header/Header'
import Slider from './../Slider/Slider'
import Cart from './../Cart/Cart'

class Home extends Component {
  render() {
    return (
        <div>
        <Header/>
        <Slider/>
        <Cart/>
      </div>
    );
  }
}

export default Home;